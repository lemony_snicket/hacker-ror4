Rails.application.routes.draw do
  #get 'comments/new'

  #get 'comments/create'

  #get 'comments/destroy'

  get 'comments', to: "comments#index", as: "comments"

  devise_for :users

  root 'articles#index'
  resources :articles do
    member do
      put 'like',    to: "articles#upvote"
      put 'dislike', to: "articles#downvote"
    end
    collection do
      # latest articles 'New'
      get 'latest',  to: "articles#latest"
    end
    resources :comments, shallow: true do
      member do
        put 'like',    to: "comments#upvote"
        put 'dislike', to: "comments#downvote"
      end
    end
  end

  resources  :users do
    get 'comments', on: :member
    get 'articles', on: :member
  end


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
