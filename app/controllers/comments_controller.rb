class CommentsController < ApplicationController
  before_action :authenticate_user!, except: [ :index, :show ]

  # all recent comments, on Comment link
  def index
    @comments = Comment.order("created_at DESC")
  end
  def create
    @article = Article.find(params[:article_id])
    # I need to allow user_id here too since i am updating it as a hidden field.
    # Or else I could have added it here ??
    @comment = @article.comments.create(params[:comment].permit(:name, :body, :user_id))
    respond_to do |format|
      format.html { redirect_to article_path(@article) }
      format.json { head :no_content }
      format.js
    end

    #redirect_to article_path(@article)
  end
  def destroy
    #puts "PARAMS"
    #puts params.inspect
    #@article = Article.find(params[:article_id])
    @comment = Comment.find(params[:id])
    @article = @comment.article
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to article_path(@article) }
      format.json { head :no_content }
      format.js   { render :layout => false }
    end
  end
  def upvote
    @comment = Comment.find(params[:id])
    if current_user.liked? @comment
      @comment.unliked_by current_user
    else
      @comment.upvote_by current_user
    end

    @article = @comment.article
    respond_to do |format|
      format.html { redirect_to article_path(@article) }
      format.json { head :no_content }
      format.js
    end
  end
  def downvote
    @comment = Comment.find(params[:id])
    #@comment.downvote_by current_user
    if current_user.disliked? @comment
      @comment.undisliked_by current_user
    else
      @comment.downvote_by current_user
    end
    @article = @comment.article
    respond_to do |format|
      format.html { redirect_to article_path(@article) }
      format.json { head :no_content }
      format.js
    end
  end

end
