class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end
  def comments
    @comments = current_user.comments.order("created_at DESC")
  end

end
