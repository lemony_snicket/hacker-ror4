class ArticlesController < ApplicationController
  before_action :get_article, only: [ :show, :edit, :destroy , :update, :upvote, :downvote]
  before_action :authenticate_user!, except: [ :index, :show , :latest]

  def index
    #@articles = Article.order("created_at DESC")
    #@articles = Article.order("created_at DESC")
    #<%= post.weighted_average.round(2) %>
    @articles = Article.order(:cached_weighted_total => :desc)
    #@articles = Article.order(:score)
  end
  # called by clicking New on _header
  def latest
    @articles = Article.order("created_at DESC")
    #<%= post.weighted_average.round(2) %>
  end
  def new
    @article = current_user.articles.new
  end
  def create
    @article = current_user.articles.build(article_params)
    if @article.url.blank? and @article.body.blank?
      flash[:error] = "One of URL or text must be entered"
      render 'new'
      return
    end
    if @article.save
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
  end
  def show
  end
  def update
    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end
  def destroy
    @article.destroy
    redirect_to articles_path
  end
  def upvote
    if current_user.liked? @article
      @article.unliked_by current_user
    else
      @article.upvote_by current_user
    end
    #redirect_to articles_path
    redirect_to :back
  end
  def downvote
    if current_user.disliked? @article
      @article.undisliked_by current_user
    else
      @article.downvote_by current_user
    end
    redirect_to :back
  end

  private
    def article_params
      params.require(:article).permit(:title, :url, :body, :tldr)
    end
    def get_article
      @article = Article.find(params[:id])
      #if @article.url.blank?
        #@article_url = article_path(@article)
      #end
    end
end
