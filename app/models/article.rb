class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments, dependent: :destroy
  validates :title, presence: true
  validate  :url_body
  acts_as_votable
  def score
      self.get_upvotes.size - self.get_downvotes.size
  end
  private
    # one of url or body should be entered
     def url_body
       if url.blank? and body.blank?
         errors.add(:url, " One of url or body should be entered.")
       end
     end
end
