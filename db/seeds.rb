# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

ActsAsVotable::Vote.delete_all
Comment.delete_all
Article.delete_all
User.delete_all

u1 = User.create!(name: 'Sentinel', email: 'zxcv@email.com', password: 'password', password_confirmation: 'password')
u2 = User.create!(name: 'Rusty Shackleford', email: 'rusty@email.com', password: 'password', password_confirmation: 'password')
names = [
  'Stephen Hawking',
  'Matz',
  'Albert E.',
  'Carl Sagan',
  'Isaac Asimov',
  'Arthur C. Clarke',
  'Stanley Kubrick',
  'Ray Bradbury',
  'jkepler'
]
names.each_with_index do |name, i|
  User.create!(name: name, email: "ex#{i}@email.com", password: 'password', password_confirmation: 'password')
end


puts "creating articles"

u1.articles.create(title: 'Advanced search with Ransack', url: 'https://www.sitepoint.com/advanced-search-ransack/')
u2.articles.create(title: 'CSS Bootstrap', url: 'http://getbootstrap.com/css/')
u1.articles.create(title: 'Bootstrap tutorial', url: 'http://www.w3schools.com/bootstrap/default.asp')
u2.articles.create(title: 'Simple Form', url: 'https://github.com/plataformatec/simple_form')
u1.articles.create(title: 'This is a Hacker News Clone in ROR4', body: 'Lets have some feedback from you fellers')
u1.articles.create(title: 'Rails DB', url: 'https://github.com/igorkasyanchuk/rails_db')
u1.articles.create(title: 'acts as votable', url: 'https://github.com/ryanto/acts_as_votable')
u2.articles.create(title: 'Rails and Ajax - dead easy', url: 'https://coderwall.com/p/kqb3xq/rails-4-how-to-partials-ajax-dead-easy')

u1.articles.create(title: 'Basic Ajax on Rails', url: 'https://richonrails.com/articles/basic-ajax-in-ruby-on-rails')
links = File.read("db/links.txt").split("\n")
unless links
  puts "No links read from db/links.txt"
  exit 1
end
puts "links read: #{links.size}"
# http://www.tutorialspoint.com/ruby-on-rails/rails-and-ajax.htm
# https://github.com/plataformatec/devise
# http://haml.info/tutorial.html
# http://www.rubyonrails365.com/7-must-have-gems-to-install-on-any-project/
# https://github.com/hothero/awesome-rails-gem
# https://github.com/rroblak/seed_dump/releases
# https://gist.github.com/seyhunak/7843549
# http://www.infoplease.com/ipea/A0931686.html
# http://getbootstrap.com/components/
#

a = Article.first
users = User.all
links.each do |li|
  ui = users.sample
  elem = li.split("\t")
  ui.articles.create(title: elem[1], url: elem[0])
end
# all users vote for first article
puts "Creating votes for first article"
users.each do |u|
  a.upvote_by u
end
usercount = users.count
arts = Article.all

# for each article, get a random number of users and get them to vote for it
puts "Creating random votes for each article"
arts.each do |art|
  c = rand(usercount) + 1
  _users = users.sample(c)
  _users.each do |u|
    art.upvote_by u
  end
end
puts "Creating comments "
arts.sample(arts.count - 4).each do |art|
  c = rand(usercount) + 1
  _users = users.sample(c)
  _users.each do |u|
    art.comments.create(body: "Hey, this was cool", user: u)
  end
end
puts "DONE"
