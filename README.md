== README

    rails generate devise:install
    rails g devise:views
    rails generate simple_form:install --bootstrap

    rails g devise User

add t:string name to user model file

    rails g model Article title:string url:string body:text tldr:text user:references

    rails g model Comment body:text user:references article:references

If you have added a field to User which is not null, registration will not be able to save. Add the field to the new.html.erb and add the following code to application_controller:

    before_action :configure_permitted_parameters, if: :devise_controller?

    protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :password, :password_confirmation, :email])
    end

In application.html.erb replace yield with:

    <div class="container">
        <% flash.each do | message_type, message | %>
            <div class="alert alert-<%= message_type %>"><%= message %></div>
        <% end %>
      <%= yield %> 
      <%= debug(params) if Rails.env.development? %>
     </div>


https://masteruby.github.io/weekly-rails/2014/08/05/how-to-add-voting-to-rails-app.html#.V27Smq6m22w


    rails g controller comments new create destroy index

deleted routes it put in config/routes.rb

https://rails-hn.herokuapp.com/ | https://git.heroku.com/rails-hn.git

If you want to put the Add Comment box first and not get an error, then create that comment @comment in the`def show` or else put @comments = @article.comments in `def show` and render @comments.
